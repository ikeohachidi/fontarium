package main

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/ikeohachidi/fontram/api"
	log "github.com/sirupsen/logrus"
)

func main() {
	r := chi.NewRouter()

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Ikeoha Chidi is responsible"))
	})

	// font routes
	r.Get("/s", api.StyleSheet)
	r.Get("/f", api.GetFont)
	r.Get("/all", api.GetAllFonts)

	// authentication routes
	r.Post("/register", api.Register)
	r.Post("/authenticate", api.Authenticate)

	if err := http.ListenAndServe(":8765", r); err != nil {
		log.Fatal("[Error]: couldn't connect to port")
	}
}
