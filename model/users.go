package model

import (
	"fmt"
)

// User represents the expected request body during a signup request
type Credentials struct {
	ID       int    `json:"id,omitempty"`
	Name     string `json:"fullname"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

// CreateUser inserts a newly signed up user's data into the database
func (d *Database) CreateUser(cred Credentials) error {
	query := fmt.Sprintf(`INSERT INTO users(email, password, name) VALUES('%v', '%v', '%v')`, cred.Email, cred.Password, cred.Name)
	statement, err := d.db.Prepare(query)
	if err != nil {
		return err
	}
	defer statement.Close()

	_, err = statement.Exec()
	if err != nil {
		return err
	}
	return nil
}

// GetUser retrieves a users data from the db
func (d *Database) GetUser(cred Credentials) (*Credentials, error) {
	query := fmt.Sprintf(`SELECT id, email, password, name FROM users WHERE email='%v'`, cred.Email)
	statement, err := d.db.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer statement.Close()

	user := new(Credentials)
	err = statement.QueryRow().Scan(&user.ID, &user.Email, &user.Password, &user.Name)
	if err != nil {
		return nil, err
	}

	return user, nil
}
