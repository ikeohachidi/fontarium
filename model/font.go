package model

import (
	"fmt"

	"github.com/lib/pq"
)

// all database operations for fonts are here
// the Database struct is inside conn.go

type FontData struct {
	Name      string         `json:"name"`
	Extension string         `json:"extenstion"`
	Category  string         `json:"category"`
	Creator   string         `json:"creator"`
	Variants  pq.StringArray `json:"variants"`
}

// Variant represents the json structure of font_data.variants json[]
type Variant struct {
	Weight int    `json:"weight"`
	Style  string `json:"string"`
	Name   string `json:"name"`
}

// GetAllFonts retrieves all fonts from the database
func (d *Database) GetAllFonts() ([]FontData, error) {
	query := fmt.Sprintf(`SELECT name, extension, category, creator, variants::json[] FROM font_data`)
	statement, err := d.db.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer statement.Close()

	rows, err := statement.Query()
	if err != nil {
		return nil, err
	}

	fontsData := make([]FontData, 0)
	for rows.Next() {
		var fontData = FontData{}
		if err = rows.Scan(&fontData.Name, &fontData.Extension, &fontData.Category, &fontData.Creator, &fontData.Variants); err != nil {
			return nil, err
		}
		fontsData = append(fontsData, fontData)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return fontsData, nil
}
