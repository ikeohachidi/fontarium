package model

import (
	"database/sql"
	"log"
	"os"

	// postgres driver
	_ "github.com/lib/pq"
)

// Database provides a connection to the database
type Database struct {
	db *sql.DB
}

// Connect connects to a postgres database
func Connect() (*Database, error) {
	connStr := "user=chidi dbname=fontram sslmode=disable password=" + os.Getenv("PSQL_PASS")

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	return &Database{db: db}, nil
}

// Ping provides simple pinging of the database
func (d *Database) Ping() {
	err := d.db.Ping()
	if err != nil {
		log.Fatalf("couldn't ping db: %v", err)
	}
}
