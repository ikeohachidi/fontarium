package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/ikeohachidi/fontram/model"
	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

// Response describes the json format for sending simple
type Response struct {
	Type bool   `json:"type"`
	Text string `json:"text"`
}

func access(w http.ResponseWriter) {
	w.Header().Set("Access-Control-Allow-Origin", "http://localhost:8080")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
}

// AuthMiddleWare checks to see if the request comes from an authenticated cookie
func AuthMiddleWare(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		session, err := store.Get(r, "ram-sesh")
		if err != nil {
			log.Error(err)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(Response{Type: false, Text: "An error occured"})
			return
		}

		if _, ok := session.Values["user_email"]; ok {
			if _, ok := session.Values["user_id"]; ok {
				next(w, r)
			} else {
				return
			}
		} else {
			return
		}
	}
}

// Register handles requests to /register route
func Register(w http.ResponseWriter, r *http.Request) {
	access(w)
	var user model.Credentials

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Error(err)
		return
	}

	err = json.Unmarshal(body, &user)
	if err != nil {
		w.WriteHeader(http.StatusNotAcceptable)
		json.NewEncoder(w).Encode(Response{Type: false, Text: "Couldn't create account"})

		log.Error("couldn't unmarshall data %v", err)
		return
	}

	encPass, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.MinCost)
	if err != nil {
		w.WriteHeader(http.StatusNotAcceptable)
		json.NewEncoder(w).Encode(Response{Type: false, Text: "Couldn't create account"})

		log.Error("couldn't unmarshall data %v", err)
		return
	}
	user.Password = string(encPass)

	err = db.CreateUser(user)
	if err != nil {
		log.Error(err)
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(Response{Type: true, Text: "Succesfully Created account"})
}

// Authenticate handles requests to /authenticate route
func Authenticate(w http.ResponseWriter, r *http.Request) {
	access(w)

	var user model.Credentials

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(Response{Type: false, Text: "An error occured"})
		return
	}

	err = json.Unmarshal(body, &user)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(Response{Type: false, Text: "An error occured"})
		return
	}

	authUser, err := db.GetUser(user)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(Response{Type: false, Text: "Incorrect email or password"})
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(authUser.Password), []byte(user.Password))
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(Response{Type: false, Text: "Incorrect email or password"})
		return
	}

	session, err := store.Get(r, "ram-sesh")
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(Response{Type: false, Text: "An error occured"})
		return
	}

	session.Values["user_email"] = authUser.Email
	session.Values["user_id"] = authUser.ID

	session.Save(r, w)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(Response{Type: false, Text: "An error occured"})
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(Response{Type: true, Text: "Login successful"})
}
