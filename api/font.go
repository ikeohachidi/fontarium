package api

import (
	"encoding/json"
	"fmt"
	"strconv"

	"html/template"

	"io/ioutil"
	"net/http"
	"net/url"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
)

// FontRequest structure for endpoints with font data queries
type FontRequest struct {
	Name    string
	Weights []string
}

// FontData consumes the json structure for data.json which is found
// in a fonts root folder
type FontData struct {
	Name      string `json:"name"`
	Extension string `json:"extension"`
	Category  string `json:"category"`
	Creator   string `json:"creator"`
	Variants  []struct {
		Weight int    `json:"weight"`
		Style  string `json:"style"`
		Name   string `json:"name"`
	}
}

const allowAccess = "*"

// StyleSheet handles requests to host/s
// It's sole purpose is to create css file that will retrieve selected fonts
// api request format: host/s?font=<fontname>:<fontweight>
func StyleSheet(w http.ResponseWriter, r *http.Request) {
	var fontRequest []FontRequest
	fontQuery := r.URL.Query()["font"]

	for _, i := range fontQuery {
		queryPart := strings.Split(i, ":")
		fontRequest = append(fontRequest, FontRequest{
			Name:    queryPart[0],
			Weights: strings.Split(queryPart[1], ","),
		})
	}

	stylesheet, err := filepath.Abs("api/fontarium.css")
	if err != nil {
		log.Errorf("Error getting filepath \n%v", err)
	}

	tFuncs := template.FuncMap{
		"getWeight": func(data string) string {
			return data[:3]
		},
		"isItalic": func(data string) bool {
			if strings.HasSuffix(data, "i") {
				return true
			}
			return false
		},
		"encode": func(data string) string {
			return url.PathEscape(data)
		},
	}

	file, err := template.New("fontarium.css").Funcs(tFuncs).ParseFiles(stylesheet)
	if err != nil {
		log.Errorf("Error parsing \n%v", err)
	}

	w.Header().Set("Access-Control-Allow-Origin", allowAccess)
	w.Header().Set("Content-Type", "text/css; charset=utf-8")

	if err := file.Execute(w, fontRequest); err != nil {
		log.Errorf("Couldn't parse file \n%v", err)
	}
}

// GetFont handles requests to host/f
// gets a single font from the file system and sends it back as response
// api request format: host/f?family=<fontname>&weight=<fontweight>
func GetFont(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()

	fontName := query["family"][0]
	fontWeight := query["weight"][0]

	italic := false
	if strings.HasSuffix(fontWeight, "i") {
		italic = true
	}

	fontPath, err := filepath.Abs("fonts/" + strings.ToLower(fontName))
	if err != nil {
		log.Errorf("Error finding font on the path: %v", err)
	}

	fontData, err := readJSON(fontPath)
	if err != nil {
		log.Errorf("error reading json %v", err)
	}

	// going through the associated data.json file for the font and retrieving
	// the name associated with the condition
	var weightName string
	for _, i := range fontData.Variants {
		if italic {
			if i.Style == "italic" && strconv.Itoa(i.Weight)[:1] == fontWeight[:1] {
				weightName = i.Name
			}
		} else if strconv.Itoa(i.Weight)[:1] == fontWeight[:1] {
			weightName = i.Name
		}
	}

	file, err := ioutil.ReadFile(fontPath + "/" + weightName)
	if err != nil {
		log.Errorf("error reading font file from folder: %v", err)
	}

	w.Header().Set("Access-Control-Allow-Origin", allowAccess)
	w.Header().Add("Access-Control-Allow-Credentials", "*")
	w.Header().Set("Content-Type", "application/octet-stream")
	w.Write(file)
}

// GetAllFonts reads all font from the server
func GetAllFonts(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", allowAccess)
	w.Header().Add("Access-Control-Allow-Credentials", "*")
	w.Header().Add("Content-Type", "application/json")

	fonts, err := db.GetAllFonts()
	if err != nil {
		log.Errorf("error getting fonts from database: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(fonts)
	if err != nil {
		log.Errorf("error writing to json response: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(Response{Type: false, Text: "Error in request"})
		return
	}
}

// readJSON is a helper function which navigates to a fonts root folder and reads
// it's associated data.json file and returns it JSON decoded
func readJSON(path string) (FontData, error) {
	var fontData FontData
	var err error
	var file []byte

	if !strings.HasSuffix(path, "/") {
		file, err = ioutil.ReadFile(path + "/data.json")
	} else {
		file, err = ioutil.ReadFile(path + "data.json")
	}

	if err != nil {
		return fontData, fmt.Errorf("couldn't read font family's associated data.json file: %v", err)
	}

	if err = json.Unmarshal(file, &fontData); err != nil {
		return fontData, fmt.Errorf("couldn't unmarshal data.json file for font: %v", err)
	}

	return fontData, nil
}
