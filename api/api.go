package api

import (
	"os"

	goSession "github.com/gorilla/sessions"
	"github.com/ikeohachidi/fontram/model"
	log "github.com/sirupsen/logrus"
)

var (
	db    *model.Database
	err   error
	store = goSession.NewCookieStore([]byte(os.Getenv("SESSION_KEY")))
)

func init() {
	// open database connection
	db, err = model.Connect()
	if err != nil {
		log.Fatalf("an error occured %v", err)
	}
}
