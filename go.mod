module github.com/ikeohachidi/fontram

go 1.14

require (
	github.com/go-chi/chi v4.1.0+incompatible
	github.com/google/uuid v1.1.1
	github.com/gorilla/sessions v1.2.0
	github.com/lib/pq v1.3.0
	github.com/sirupsen/logrus v1.5.0
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71
)
